import pymysql

from flask import Flask, jsonify, request
from flask_cors import CORS
import pymysql

app = Flask(__name__)
cors = CORS(app)

@app.route('/users', methods=['GET'])
def get_users():
    # To connect MySQL database
    conn = pymysql.connect(host='localhost', user='root', password = '', db='database2')

    cur = conn.cursor()
    cur.execute("SELECT * FROM users")
    output = cur.fetchall()

    #print(type(output)); #this will print tuple

    #for rec in output:
       # print(rec);

    # To close the connection
    conn.close()

    return jsonify(output);



@app.route('/users', methods=['POST'])
def insertRecord():
    conn= pymysql.connect(host='localhost',user='root',password= '',db='database2')

    #get raw json values
    raw_json = request.get_json();
    name= raw_json['name'];
    age= int(raw_json['age']);
    city = raw_json['city'];
    cur= conn.cursor();
    cur.execute(f"INSERT INTO users (id,name,age,city) VALUES (NULL,'{name}','{age}','{city}')");
    conn.commit();
    return {"result" : "Record inserted Succesfully"}

@app.route('/users', methods=['PUT'])
def updateRecord():
    conn= pymysql.connect(host='localhost',user='root',password= '',db='database2')

    raw_json = request.get_json();
    id = int(request.args.get('id'))
    raw_json = request.get_json();
    name= raw_json["name"];
    age= int(raw_json["age"]);
    city = raw_json["city"];
    cur = conn.cursor()
    cur.execute(f"UPDATE users SET name = '{name}',age='{age}',city='{city}' WHERE id = '{id}'");
    conn.commit()
    return {"result" : "Record updated Succesfully"}


if __name__ == "__main__":
    app.run(debug=True);
