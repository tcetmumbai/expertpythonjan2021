#parent class
class Grandfather():
    def __init__(self,grandfathername):
        self.grandfathername=grandfathername
#intermediate class
class Father(Grandfather):
    def __init__(self,fathername,grandfathername):
        self.fathername=fathername

        Grandfather.__init__(self,grandfathername)
#child class
class Son(Father):
    def __init__(self,sonname,fathername,grandfathername):
        self.sonname=sonname

        Father.__init__(self,fathername,grandfathername)

    def print_name(self):
        print({self.grandfathername})
        print({self.fathername})
        print({self.sonname})

s1=Son("Prince","Rampal","Lal Mani")
s1.print_name()
