
def Fact(n):
    if n>=1:
        op=n*Fact(n-1)
        return op
    else:
        return 1
print(Fact(5))

#5*Fact(4)
#5*4*Fact(3)
#5*4*3*Fact(2)
#5*4*3*2*Fact(1)
#5*4*3*2*1*Fact(0)


