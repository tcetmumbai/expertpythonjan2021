def func1(a,b,c):
    return a+b+c

print(func1(3,6,7))

#func1(11, 9)
#20


def person(name,age,city,salary=0):
    print(f"{name}")
    print(f"{age}")
    print(f"{city}")
    print(f"{salary}")

person(name="Avinash", age=20,city="Mumbai")
person(name="Utsav", age=20,city="Mumbai",salary=1000)    
